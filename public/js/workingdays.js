
function doIt(){

    jQuery("#confluencedays").empty();
    jQuery("#messages").empty();

    var tableFormat = jQuery('input[name=reportFormat]:checked').val();

    var includeTitles = false;
    var includeLinks = false;
    var jiraData = null;
    var withTickets = false;
    var createdTickets = false;
    var completedTickets = false;

    if(jQuery("#includeTicketTitles").is(':checked')){
        includeTitles = true;
    }

    if(jQuery("#includeTicketLinks").is(':checked')){
        includeLinks = true;
    }

    if (jQuery('#checkJira').is(':checked')){

        var ticketStatus = jQuery('input[name=ticketStatus]:checked').val();
        if(ticketStatus == "created"){

            createdTickets = true;
            completedTickets = false;
        }
        else{
            createdTickets = false;
            completedTickets = true;
        }
    }

    if(checkIfFormIsValid()){

        //Jira and maybe github
        if (jQuery('#checkJira').is(':checked')){

            var user = jQuery("#user").val();
            var pass = jQuery("#pass").val();

            insertMessage("INFO","Getting data from Jira");
            withTickets = true;
            getDataFromJira(createdTickets, completedTickets);
        }
        //Jira NO, GitHub Yes
        else if(!jQuery('#checkJira').is(':checked') && jQuery('#checkGithub').is(':checked')){
            insertMessage("INFO","Excluding Jira data");
            insertMessage("INFO","Getting data from GitHub");

            getDataFromGitHub(null);

        }
        else{

            insertMessage("INFO","Creating table with dates only");
            createTable(null, null);
        }

    }
}



function selectCurrentYearAndMonth(){

    var d = new Date();
    var currMonth = d.getMonth();
    var currYear = d.getFullYear();

    jQuery("#month option[value="+currMonth+"]").attr("selected", "selected");
    jQuery("#year option[value="+currYear+"]").attr("selected", "selected");

}

//dataFromJira, dataFromGithub
function createTable(dataFromJira, dataFromGithub) {

    var tableFormat = jQuery('input[name=reportFormat]:checked').val();
    var withTickets = false;
    var includeTitles = false;
    var includeLinks = false;
    var withCommits = false;
    var limit = jQuery("#maxCommits").val();

    if(limit == "ALL") limit = null;

    if(jQuery('#checkJira').is(':checked')){
        withTickets = true;
    }
    if(jQuery("#includeTicketTitles").is(':checked')){
        includeTitles = true;
    }

    if(jQuery("#includeTicketLinks").is(':checked')){
        includeLinks = true;
    }
    if(jQuery('#checkGithub').is(':checked')){
        withCommits = true;
    }

    console.log(tableFormat, withTickets, dataFromJira, includeTitles, includeLinks);

    jQuery("#confluencedays").empty();

    var table = jQuery("<table border='1'>");
    var month_sel = jQuery("#month").val();
    var month_text_sel = jQuery("#month option:selected").text();
    var year_sel = jQuery("#year").val();
    var res = getWorkingDaysInMonth(month_sel, year_sel);

    var tableHeader = jQuery("<thead>");
    var thRow = "<th>" + month_text_sel + " / " + year_sel + "</th>";

    if(!withTickets || includeTitles){
        thRow += "<th>Tasks</th>";
    }

    if(includeLinks){
        thRow += "<th>Jira links</th>";
    }

    if(withCommits){
        thRow += "<th>Commits</th>";
    }

    var tableHeaderRow = jQuery(thRow);
    tableHeader.append(tableHeaderRow);

    table.append(tableHeader);

    var tableBody = jQuery("<tbody>");

    var tableRow = "";
    var dateColumn = "";
    var ticketColumn = "";

    if(tableFormat == "days"){


        for (i = 0; i < res.length; i++) {
            var dayN = res[i].getDate();
            var dayNumber = getDayNumberString(res[i].getDate());
            var dayName = getDayNameString(res[i].getDay());
            var dayToDisplay = dayNumber + " / " + dayName;


            var finishedTasks = getFinishedTasks(dataFromJira, dayN);
            var finishedTasksLinks = getFinishedTaskLinks(dataFromJira, dayN);
            var commits = "";



            tableRow = "<tr><td>" + dayToDisplay + "</td>";

            if(!withTickets){

                tableRow +="<td></td>";
            }
            if(includeTitles && withTickets){

                tableRow += "<td>"+finishedTasks+"</td>";
            }
            if(includeLinks){

                tableRow += "<td>"+finishedTasksLinks+"</td>";
            }
            if(withCommits){

                commits = getCommitsByDay(dataFromGithub, dayN, limit);
                tableRow += "<td>"+commits+"</td>";
            }

            tableRow +="<tr>";
            tableBody.append(tableRow);
        }

    }
    else{
        var finishedTasks = "";
        var finishedTasksLinks = "";
        var commits = "";

        for (i = 0; i < res.length; i++) {
            var dayN = res[i].getDate();
            var dayNumber = getDayNumberString(res[i].getDate());
            var dayName = getDayNameString(res[i].getDay());
            var dayToDisplay = dayNumber + " / " + dayName;

            finishedTasks += getFinishedTasks(dataFromJira, dayN);
            finishedTasksLinks += getFinishedTaskLinks(dataFromJira, dayN);
            commits += getCommitsByDay(dataFromGithub, dayN, limit);

            if(dateColumn == ""){
                dateColumn += dayToDisplay;

            }

            if(dayName == "Friday" && dateColumn ==""){

                dateColumn = dayToDisplay;
                tableRow = "<tr><td>" + dateColumn + "</td>";

                if(!withTickets){

                    tableRow +="<td></td>";
                }
                if(includeTitles && withTickets){

                    tableRow += "<td>"+finishedTasks+"</td>";
                }
                if(includeLinks){

                    tableRow += "<td>"+finishedTasksLinks+"</td>";
                }
                if(withCommits){

                    tableRow += "<td>"+commits+"</td>";
                }
                tableRow +="<tr>";

                tableBody.append(tableRow);
                tableRow = "";
                finishedTasks = "";
                finishedTasksLinks = "";
                dateColumn = "";
                ticketColumn = "";
                commits = "";
            }
            else if(dayName == "Friday" && dateColumn != ""){

                dateColumn += " - "+dayToDisplay;

                tableRow = "<tr><td>" + dateColumn + "</td>";

                if(!withTickets){

                    tableRow +="<td></td>";
                }
                if(includeTitles && withTickets){

                    tableRow += "<td>"+finishedTasks+"</td>";
                }
                if(includeLinks){

                    tableRow += "<td>"+finishedTasksLinks+"</td>";
                }
                if(withCommits){

                    tableRow += "<td>"+commits+"</td>";
                }

                tableRow +="<tr>";
                tableBody.append(tableRow);
                tableRow = "";
                finishedTasks = "";
                finishedTasksLinks = "";
                dateColumn = "";
                ticketColumn = "";
                commits = "";
            }

            if(i == res.length -1 && dateColumn !=""){
                dateColumn += " - "+dayToDisplay;

                tableRow = "<tr><td>" + dateColumn + "</td>";

                if(!withTickets){

                    tableRow +="<td></td>";
                }
                if(includeTitles && withTickets){

                    tableRow += "<td>"+finishedTasks+"</td>";
                }
                if(includeLinks){

                    tableRow += "<td>"+finishedTasksLinks+"</td>";
                }
                if(withCommits){

                    tableRow += "<td>"+commits+"</td>";
                }
                tableRow +="<tr>";
                tableBody.append(tableRow);
            }
        }



    }


    table.append(tableBody);
    jQuery("#confluencedays").append(table);

}

function getDayNumberString(d) {

    if (d == 1 || d == 21) return d + "st";
    if (d == 2 || d == 22) return d + "nd";
    if (d == 3 || d == 23) return d + "rd";

    return d + "th";
}

function getDayNameString(d) {

    var dayNames = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];

    return dayNames[d];
}


function getWorkingDaysInMonth(month, year) {
    var date = new Date(year, month, 1);

    var day;
    var daysToReturn = [];

    while (date.getMonth() == month) {
        day = date.getDay();

        if (day !== 0 && day !== 6) {
            daysToReturn.push(new Date(date));
        }

        date.setDate(date.getDate() + 1);
    }
    return daysToReturn;
}

function getDataFromJira(createdTickets, completedTickets) {

    var user = jQuery("#user").val();
    var pass = jQuery("#pass").val();
    var filteredData = null;
    var jiraUrl = "https://www.ebi.ac.uk/panda/jira/rest/api/2/search?";

    if(createdTickets){

        jiraUrl += "jql=reporter=" + user;
    }
    else{

        jiraUrl += "jql=assignee=" + user;
    }

    jQuery.ajax({
        url: jiraUrl,
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Authorization", "Basic " + btoa(user + ":" + pass));
        }, success: function (data) {

            filteredData = filterData(data, createdTickets, completedTickets, user);
            console.log(JSON.stringify(filteredData));

            if(jQuery('#checkGithub').is(':checked')){

                getDataFromGitHub(filteredData);
            }
            else{

                createTable(filteredData, null);
            }



        }
    })

}

function filterData(data, createdTickets, completedTickets, user){

    var filteredData = [];
    var month_sel = jQuery("#month").val();
    var year_sel = jQuery("#year").val();
    //if(data.hasAttribute("issues")){

    var issues = data["issues"];
    console.log("cre:"+createdTickets);
    for(var i=0;i<issues.length;i++){

        var issueObj = issues[i]
        var issueFieldsObj = issueObj["fields"];
        var statusObj = issueFieldsObj["status"];
        var status = statusObj["name"];
        var creatorObj = issueFieldsObj["reporter"];

        if(completedTickets && (status == "Resolved" || status == "Closed")){

            var issuekey = issueObj["key"];
            var issuename = issueFieldsObj["summary"];
            var resdate = issueFieldsObj["resolutiondate"];

            if(resdate != null || resdate !== undefined){

                var res = resdate.split("T");
                var r = res[0].split("-");
                var resday = r[2];
                var resmonth = r[1];
                var resyear = r[0];
                var month_sel_int = parseInt(month_sel);
                month_sel_int++
                if(parseInt(resmonth) == month_sel_int && parseInt(resyear) == parseInt(year_sel)){

                    filteredData.push({
                        key: issuekey,
                        name:issuename+" (CLOSED)",
                        date:resday
                    });
                }

            }
        }
        if(createdTickets){

            var issuekey = issueObj["key"];
            var issuename = issueFieldsObj["summary"];
            var creator = creatorObj["name"];
            var crdate = issueFieldsObj["created"];
            console.log(creator, user, issuekey);
            if(creator == user){


                if(crdate != null || crdate !== undefined){

                    var res = crdate.split("T");
                    var r = res[0].split("-");
                    var crday = r[2];
                    var crmonth = r[1];
                    var cryear = r[0];
                    var month_sel_int = parseInt(month_sel);
                    month_sel_int++
                    if(parseInt(crmonth) == month_sel_int && parseInt(cryear) == parseInt(year_sel)){

                        filteredData.push({
                            key: issuekey,
                            name:issuename+" (CREATED)",
                            date:crday
                        });
                    }

                }

            }
        }
    }
    //}

    return filteredData;
}


function getFinishedTasks(filteredData, currentDay){

    var str = "";
    if (filteredData == null) return str;

    for(var i=0;i<filteredData.length;i++){

        var day = filteredData[i]["date"];
        var name = filteredData[i]["name"];
        var key = filteredData[i]["key"];

        if(parseInt(currentDay) == parseInt(day)){
            str += key+" "+name+"<br/>";
        }
    }

    return str;
}

function getFinishedTaskLinks(filteredData, currentDay){

    var baseUrl = "https://www.ebi.ac.uk/panda/jira/browse/"
    var str = "";
    if (filteredData == null) return str;

    for(var i=0;i<filteredData.length;i++){

        var key = filteredData[i]["key"];
        var day = filteredData[i]["date"];

        if(parseInt(currentDay) == parseInt(day)){
            str += baseUrl+key+"<br/>";
        }
    }

    return str;
}

function getCommitsByDay(filteredData, currentDay, limit){

    var str = "";
    var commitCounter = 0;

    if (filteredData == null) return str;

    for(var i=0;i<filteredData.length;i++){

        var day = filteredData[i]["date"];
        var name = filteredData[i]["name"];

        if(parseInt(currentDay) == parseInt(day)){
            str += name+"<br/>";
            commitCounter++;
        }

        if (limit != null &&  commitCounter >= parseInt(limit)) break;
    }

    return str;
}



function insertMessage(type, message){

    var msgField = jQuery("#messages");

    var msg = jQuery("<div/>");
    msg.addClass("alert");

    if(type == "INFO"){

        msg.addClass("alert-info");
    }
    else if(type == "ERROR"){

        msg.addClass("alert-danger");
    }
    else if(type == "SUCCESS"){

        msg.addClass("alert-success");
    }

    msg.text(message);

    msgField.append(msg);
}

function checkIfFormIsValid(){

    var isValid = true;


    if (jQuery('#checkJira').is(':checked')) {

        var user = jQuery("#user").val();
        var pass = jQuery("#pass").val();

        if (user == '' || pass == '') {
            isValid = false;
            insertMessage("ERROR", "Missing Jira username or password");

        }
    }

    if(jQuery('#checkGithub').is(':checked')){

        var gbUser = jQuery('#githubUser').val();
        var repo = jQuery('#repo').val();
        var project = jQuery('#project').val();
        var branch = jQuery('#branch').val();

        if(gbUser == '' || repo == '' || project == '' || branch == ''){

            isValid = false;
            insertMessage("ERROR", "Missing Github username, repository, project or branch");
        }

    }

    return isValid;
}

function getDataFromGitHub(dataFromJira){

    var gbUser = jQuery('#githubUser').val();
    var repo = jQuery('#repo').val();
    var project = jQuery('#project').val();
    var branch = jQuery('#branch').val();


    jQuery.ajax({
        url: 'https://api.github.com/users/'+gbUser+'/events',
        success: function (data) {

            var filteredGBData = filterGithubData(data, repo, project, branch);

            createTable(dataFromJira, filteredGBData);

        }
    })

}

function filterGithubData(data, repo, project, branch){

    var filteredData = [];
    var month_sel = jQuery("#month").val();
    var year_sel = jQuery("#year").val();
    branch = branch.toLowerCase();

    for(var i=0;i<data.length;i++){

        var gbObject = data[i];
        var repoObj= gbObject["repo"];
        var repoName = repoObj["name"];
        var eventType = gbObject["type"];
        var githubString = repo+"/"+project;
            githubString = githubString.toLowerCase();


        //skip other repositories
        if(repoName.toLowerCase() != githubString) continue;



        if(eventType == "PushEvent"){

            var payloadObj = gbObject["payload"];
            var pushDate = gbObject["created_at"];

            var gbBranch = payloadObj["ref"];
                gbBranch = gbBranch.toLowerCase();

            if(gbBranch == "refs/heads/"+branch){

                var res = pushDate.split("T");
                var r = res[0].split("-");
                var crday = r[2];
                var crmonth = r[1];
                var cryear = r[0];
                var month_sel_int = parseInt(month_sel);
                month_sel_int++
                if(parseInt(crmonth) == month_sel_int && parseInt(cryear) == parseInt(year_sel)){

                    var commitsArr = payloadObj["commits"];

                    for(var j=0;j<commitsArr.length;j++){

                        var commitMsg = commitsArr[j]["message"];
                        //remove merge messages
                        if(!commitMsg.startsWith("Merge branch")){

                            filteredData.push({
                                name:commitMsg,
                                date:crday
                            });
                        }
                    }
                }

            }


        }

    }

    console.log(JSON.stringify(filteredData));
    return filteredData;
}
